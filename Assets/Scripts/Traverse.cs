using System;


class Traverse
{
    public static void go(int x, int y, Func<int, int, bool> func1, Func<int, int, bool> func2)
    {
        if (func1(x, y))
        {
            if (func1(x - 1, y)) { func2(x - 1, y); };
            if (func1(x + 1, y)) { func2(x + 1, y); };
            if (func1(x, y - 1)) { func2(x, y - 1); };
            if (func1(x, y + 1)) { func2(x, y + 1); };
            if (func1(x + 1, y + 1)) { func2(x + 1, y + 1); };
            if (func1(x - 1, y + 1)) { func2(x - 1, y + 1); };
            if (func1(x - 1, y - 1)) { func2(x - 1, y - 1); };
            if (func1(x + 1, y - 1)) { func2(x + 1, y - 1); };
        }
    }
}
