using UnityEngine;
using System;

class Board : MonoBehaviour {

    public static GameObject gameOverScreenPublic;
    public static GameObject winningScreenPublic;
    public GameObject gameOverScreen;
    public GameObject winningScreen;

    public GameObject gridPrefab;
    float xOffset = 0f;
    float zOffset = 0f;
    public int boardSize = 36;
    public static long distanceBetweenTiles = 3L;
    System.Random rnd = new System.Random();
    public static GridPiece[,] board;

    public Board() { }

    void Start()
    {
        gameOverScreen.SetActive(false);
        winningScreen.SetActive(false);
        // Make game over available to all instances of board
        gameOverScreenPublic = gameOverScreen;
        winningScreenPublic = winningScreen;

        int x = (int)Math.Sqrt(boardSize);
        board = new GridPiece[x, x];
        populateBoard(board);
    }

    void populateBoard(GridPiece[,] board)
    {
        for (int x = 0; x < board.GetLength(0); x++)
        {
            for (int y = 0; y < board.GetLength(1); y++)
            {
                Boolean isBomb = rnd.NextDouble() < 0.2;
                var piece = createGridPiecesUI(y);
                piece.isBomb = isBomb;
                piece.x = x;
                piece.y = y;
                board[x, y] = piece;
            }
        }
        for (int x = 0; x < board.GetLength(0); x++)
        {
            for (int y = 0; y < board.GetLength(1); y++)
            {
                if (board[x, y].isBomb)
                {
                    Traverse.go(x, y, canPlace, addScore);
                }
            }
        }
    }

    GridPiece createGridPiecesUI(int y)
    {
        xOffset += distanceBetweenTiles;

        if (y == 0)
        {
            zOffset += distanceBetweenTiles;
            xOffset = 0;
        }

        return Instantiate(
            gridPrefab,
            new Vector3(
                transform.position.x + xOffset,
                transform.position.y,
                transform.position.z + zOffset
            ),
            transform.rotation
        ).GetComponent<GridPiece>();
    }

    public static Boolean isBombLeft()
    {
        Boolean isBombLeft = false;
        for (int x = 0; x < board.GetLength(0); x++)
        {
            for (int y = 0; y < board.GetLength(1); y++)
            {
                isBombLeft = board[x, y].isBomb ? !board[x, y].isFlagged : isBombLeft;
            }
        }
        if(!isBombLeft)
        {
            winningScreenPublic.SetActive(true);
        }
        return isBombLeft;
    }

    public static Boolean flagPoint(int x, int y)
    {
        board[x, y].isFlagged = true;
        return true;
    }
    public static Boolean revealPoint(int x, int y)
    {
        if (board[x, y].isBomb || board[x, y].isVisable)
        {
            return false;
        }
        print(x);
        print(y);
        board[x, y].isVisable = true;
        board[x, y].remove();
        if(board[x, y].isFlagged)
        {
            board[x, y].destroyFlag();
        }
        board[x, y].showScore();
        if (board[x, y].Score == 0)
        {
            Traverse.go(x, y, canPlace, revealPoint);
        }
        return true;
    }

    public static Boolean canPlace(int x, int y)
    {
        return x >= 0 && x < board.GetLength(0) &&
         y >= 0 && y < board.GetLength(1);
    }

    Boolean addScore(int x, int y)
    {
        board[x, y].Score += 1;
        return true;
    }

    public static void showGameOver()
    {
        gameOverScreenPublic.SetActive(true);
    }
}
