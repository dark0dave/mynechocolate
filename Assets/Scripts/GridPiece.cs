using UnityEngine;
using System;

class GridPiece : MonoBehaviour
{
    public Boolean isVisable = false;
    public Boolean isFlagged = false;
    public Boolean isBomb = false;
    public int x;
    public int y;
    public int Score = 0;

    public GameObject bombPrefab;
    public GameObject flagPrefab;
    public GameObject scorePrefab;
    
    public GridPiece(Boolean isBomb, int x, int y)
    {
        this.isBomb = isBomb;
        this.x = x;
        this.y = y;
    }

    void Update()
    {
    }

    void OnMouseDown()
    {

        if (this.isBomb)
        {
            remove();
            showBomb();
            Board.showGameOver();
        }

        if(!isVisable)
        {
            Board.revealPoint(x, y);
            remove();
            if (isFlagged)
            {
                destroyFlag();
            }
            Board.isBombLeft();
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {

            if (!isFlagged && !isVisable)
            {
                Board.flagPoint(this.x, this.y);
                showFlag();
            }

            else if (isFlagged)
            {
                destroyFlag();
                isFlagged = false;
            }

        }
    }

    public void remove()
    {
        Destroy(gameObject.transform.GetChild(0).gameObject);
    }

    public void showBomb()
    {
        Instantiate(bombPrefab, transform.position, transform.rotation);
    }

    public void showFlag()
    {
        var newFlag = Instantiate(flagPrefab, transform.position, transform.rotation);
        newFlag.transform.parent = gameObject.transform;
    }
    public void destroyFlag()
    {
        Destroy(gameObject.transform.GetChild(1).gameObject);
    }

    public void showScore()
    {
        if (Score > 0) {
            var newScore = Instantiate(scorePrefab, transform.position, transform.rotation);
            newScore.transform.GetChild(0).GetComponent<TextMesh>().text = Score.ToString();
        }
        
    }
}
